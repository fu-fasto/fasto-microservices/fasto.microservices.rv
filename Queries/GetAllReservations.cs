﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.RV.Queries
{
    public class GetAllReservations : AllQueryBase<Reservation>
    {
        public Guid? StoreId { get; set; }
        public Guid? CustomerId { get; set; }
    }
}