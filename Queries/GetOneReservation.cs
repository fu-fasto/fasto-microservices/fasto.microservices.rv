﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.RV.Queries
{
    public class GetOneReservation : OneQueryBase<Reservation, Guid>
    {
    }
}