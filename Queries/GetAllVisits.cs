﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.RV.Queries
{
    public class GetAllVisits : AllQueryBase<Visit>
    {
        public Guid? StoreId { get; set; }
        public Guid? CustomerId { get; set; }

        public Guid? PaymentTransactionId { get; set; }

        public bool? IsActivated { get; set; }
    }
}