﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.RV.Queries
{
    public class GetOneVisit : OneQueryBase<Visit, Guid>
    {
    }
}