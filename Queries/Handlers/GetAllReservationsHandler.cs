﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class GetAllReservationsHandler : IQueryHandler<GetAllReservations, IEnumerable<Reservation>>
    {
        private readonly IRepository<Reservation, Guid> _reservationRepository;

        public GetAllReservationsHandler(IRepository<Reservation, Guid> reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public Task<IEnumerable<Reservation>> Handle(GetAllReservations query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Reservation>>();
            
            if (query.StoreId.HasValue)
            {
                findQuery.Filters.Add(x => x.StoreId.Equals(query.StoreId.Value));
            }

            if (query.CustomerId.HasValue)
            {
                findQuery.Filters.Add(x => x.CustomerId.Equals(query.CustomerId.Value));
            }

            return _reservationRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}