﻿using System.Drawing;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Dtos;
using FastO.Microservices.RV.Services;
using FluentValidation;
using MicroBoost.Cqrs.Queries;
using Newtonsoft.Json;
using QRCoder;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class GetCheckInQrHandler : IQueryHandler<GetCheckInQr, CheckInQrDto>
    {
        private static QRCodeGenerator _qrGenerator = new();
        private readonly IServiceClient _serviceClient;


        public GetCheckInQrHandler(IServiceClient serviceClient)
        {
            _serviceClient = serviceClient;
        }

        public async Task<CheckInQrDto> Handle(GetCheckInQr query, CancellationToken cancellationToken)
        {
            var store = await _serviceClient.GetStore(query.StoreId, cancellationToken) ??
                throw new ValidationException($"Store {query.StoreId} not exist!");

            var payload = new
            {
                storeId = query.StoreId,
                storeName = store.Name,
                table = query.Table
            };
            var qrCodeData = _qrGenerator.CreateQrCode(JsonConvert.SerializeObject(payload), QRCodeGenerator.ECCLevel.Q);
            var qrCode = new Base64QRCode(qrCodeData);
            var qrCodeImageAsBase64 = qrCode.GetGraphic(20,Color.Black, Color.White, true, Base64QRCode.ImageType.Jpeg);
            qrCodeData.Dispose();
            
            return new()
            {
                Base64Code = qrCodeImageAsBase64,
                Type = "jpeg"
            };
        }
    }
}