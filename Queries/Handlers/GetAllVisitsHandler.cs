﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class GetAllVisitsHandler : IQueryHandler<GetAllVisits, IEnumerable<Visit>>
    {
        private readonly IRepository<Visit, Guid> _visitRepository;

        public GetAllVisitsHandler(IRepository<Visit, Guid> visitRepository)
        {
            _visitRepository = visitRepository;
        }

        public Task<IEnumerable<Visit>> Handle(GetAllVisits query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Visit>>();
            // if (query.StoreId.HasValue && query.CustomerId.HasValue)
            // {
            //     findQuery.Filter = x =>
            //         x.StoreId.Equals(query.StoreId.Value) && x.CustomerId.Equals(query.CustomerId.Value);
            // }
            // else if (query.StoreId.HasValue)
            // {
            //     findQuery.Filter = x => x.StoreId.Equals(query.StoreId.Value);
            // }
            // else if (query.CustomerId.HasValue)
            // {
            //     findQuery.Filter = x => x.CustomerId.Equals(query.CustomerId.Value);
            // }
            
            if (query.StoreId.HasValue)
            {
                findQuery.Filters.Add(x => x.StoreId.Equals(query.StoreId.Value));
            }
            if (query.CustomerId.HasValue)
            {
                findQuery.Filters.Add(x => x.CustomerId.Equals(query.CustomerId.Value));
            }
            if (query.IsActivated.HasValue)
            {
                findQuery.Filters.Add(x => x.IsActivated.Equals(query.IsActivated));
            }

            return _visitRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}