﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class GetOneVisitHandler : IQueryHandler<GetOneVisit, Visit>
    {
        private readonly IRepository<Visit, Guid> _visitRepository;

        public GetOneVisitHandler(IRepository<Visit, Guid> visitRepository)
        {
            _visitRepository = visitRepository;
        }

        public async Task<Visit> Handle(GetOneVisit request, CancellationToken cancellationToken)
        {
            var visit = await _visitRepository.FindOneAsync(request.Id, cancellationToken)
                        ?? throw new NullReferenceException($"Visit {request.Id} does not exist!");

            return visit;
        }
    }
}