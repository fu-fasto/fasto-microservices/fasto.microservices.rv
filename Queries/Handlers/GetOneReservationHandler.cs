﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class GetOneReservationHandler : IQueryHandler<GetOneReservation, Reservation>
    {
        private readonly IRepository<Reservation, Guid> _reservationRepository;

        public GetOneReservationHandler(IRepository<Reservation, Guid> reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public async Task<Reservation> Handle(GetOneReservation request, CancellationToken cancellationToken)
        {
            var reservation = await _reservationRepository.FindOneAsync(request.Id, cancellationToken)
                              ?? throw new NullReferenceException($"Reservation {request.Id} does not exist!");

            return reservation;
        }
    }
}