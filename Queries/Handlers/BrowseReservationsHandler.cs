﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;

namespace FastO.Microservices.RV.Queries.Handlers
{
    public class BrowseReservationsHandler : IQueryHandler<BrowseReservations, PagedResult<Reservation>>
    {
        private readonly IRepository<Reservation, Guid> _reservationRepository;

        public BrowseReservationsHandler(IRepository<Reservation, Guid> reservationRepository)
        {
            _reservationRepository = reservationRepository;
        }

        public Task<PagedResult<Reservation>> Handle(BrowseReservations query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<Reservation>>();
            var rawDict =
                new Dictionary<string, Expression<Func<Reservation, object>>>(StringComparer.OrdinalIgnoreCase)
                {
                    {"PeopleNumber", x => x.PeopleNumber},
                    {"ExpectedTime", x => x.ExpectedTime}
                };

            query.RawSorts?.ForEach(rawSort =>
            {
                if (rawDict.TryGetValue(rawSort.Field, out var selector))
                    findQuery.SortExpressions.Add(new SortExpression<Reservation>
                    {
                        Selector = selector,
                        IsAscending = rawSort.IsAscending
                    });
            });

            return _reservationRepository.BrowseAsync(findQuery, cancellationToken);
        }
    }
}