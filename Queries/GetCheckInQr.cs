﻿using System;
using FastO.Microservices.RV.Dtos;
using MicroBoost.Cqrs.Queries;

namespace FastO.Microservices.RV.Queries
{
    public class GetCheckInQr : IQuery<CheckInQrDto>
    {
        public Guid StoreId { get; set; }

        public string Table { get; set; }
    }
}