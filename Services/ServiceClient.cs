﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Commands;
using FastO.Microservices.RV.Dtos;
using MicroBoost.Http;

namespace FastO.Microservices.RV.Services
{
    public class ServiceClient : IServiceClient
    {
        private readonly IHttpClient _httpClient;
        private readonly HttpClientOptions _clientOptions;

        public ServiceClient(IHttpClient httpClient, HttpClientOptions clientOptions)
        {
            _httpClient = httpClient;
            _clientOptions = clientOptions;
        }

        public async Task CreatePaymentTransaction(
            CreateVisitPaymentTransaction command,
            CreatePaymentTransactionDto createPaymentTransactionDto,
            CancellationToken cancellationToken = default)
        {
            switch (createPaymentTransactionDto.Provider.ToUpper())
            {
                case "PAYPAL":
                {
                    var paymentTransactionResponse = await _httpClient
                        .PostAsync<CreatePaymentTransactionDto, PayPalPaymentTransactionResponse>(
                            $"{_clientOptions.Services["PaymentMicroservice"]}/payment-transactions",
                            createPaymentTransactionDto,
                            cancellationToken);
                    command.TransactionData = paymentTransactionResponse.TransactionData;
                    command.PaymentTransactionId = paymentTransactionResponse.Id;
                    break;
                }
                
                case "MOMO":
                {
                    var paymentTransactionResponse = await _httpClient
                        .PostAsync<CreatePaymentTransactionDto, MomoPaymentTransactionResponse>(
                            $"{_clientOptions.Services["PaymentMicroservice"]}/payment-transactions",
                            createPaymentTransactionDto,
                            cancellationToken);
                    command.TransactionData = paymentTransactionResponse.TransactionData;
                    command.PaymentTransactionId = paymentTransactionResponse.Id;
                    break;
                }
                
                default:
                    throw new ArgumentException($"{command.Provider} is not supported!");
            }
        }

        public Task<List<OrderDto>> GetOrdersByVisitId(Guid visitId, CancellationToken cancellationToken = default)
        {
            return _httpClient
                .GetAsync<List<OrderDto>>(
                    $"{_clientOptions.Services["OrderMicroservice"]}/orders?visitId={visitId}",
                    cancellationToken);
        }

        public Task<StoreDto> GetStore(Guid id, CancellationToken cancellationToken = default)
        {
            return _httpClient
                .GetAsync<StoreDto>(
                    $"{_clientOptions.Services["StoreSearchMicroservice"]}/stores/{id}",
                    cancellationToken);
        }

        public Task<UserDto> GetUser(Guid id, CancellationToken cancellationToken = default)
        {
            return _httpClient
                .GetAsync<UserDto>(
                    $"{_clientOptions.Services["UserMicroservice"]}/users/{id}",
                    cancellationToken);
        }
    }
}