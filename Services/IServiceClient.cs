﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Commands;
using FastO.Microservices.RV.Dtos;

namespace FastO.Microservices.RV.Services
{
    public interface IServiceClient
    {
        Task CreatePaymentTransaction(CreateVisitPaymentTransaction command,
            CreatePaymentTransactionDto createPaymentTransactionDto,
            CancellationToken cancellationToken = default);

        Task<List<OrderDto>> GetOrdersByVisitId(Guid visitId, CancellationToken cancellationToken = default);

        Task<StoreDto> GetStore(Guid id, CancellationToken cancellationToken = default);

        Task<UserDto> GetUser(Guid id, CancellationToken cancellationToken = default);
    }
}