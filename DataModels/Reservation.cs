﻿using System;
using System.Collections.Generic;
using System.Linq;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Types;

namespace FastO.Microservices.RV.DataModels
{
    public class Reservation : EntityBase<Guid>
    {
        /// <summary>
        /// If of reservation
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Unique code of reservation
        /// </summary>
        [UniqueIndex]
        public string Code { get; set; }

        /// <summary>
        /// Id of store
        /// </summary>
        [UniqueIndex]
        public Guid StoreId { get; set; }

        /// <summary>
        /// Id of customer
        /// </summary>
        [UniqueIndex]
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Id of visit
        /// </summary>
        public Guid? VisitId { get; set; }

        /// <summary>
        /// Store name
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// Customer name
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// Number of people
        /// </summary>
        public int PeopleNumber { get; set; }

        /// <summary>
        /// Expected time of reservation
        /// </summary>
        public DateTimeOffset ExpectedTime { get; set; }

        /// <summary>
        /// Current state of reservation
        /// </summary>
        public RState State { get; set; }

        /// <summary>
        /// Note of reservation
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Reservation list of reservation
        /// </summary>
        public IList<ReservationState> States { get; set; } = new List<ReservationState>();

        public void UpdateReservationState(RState state, string note = null)
        {
            if (States.Any() && States.Last().State == state)
            {
                States.Last().Note = note;
                Note = note;

                if (States.Any() && state != RState.Created) return;
            }

            States.Add(new ReservationState()
            {
                State = state,
                Note = note
            });
            State = state;
        }
    }

    public class ReservationState
    {
        /// <summary>
        /// State
        /// </summary>
        public RState State { get; set; }

        /// <summary>
        /// Created time
        /// </summary>
        public DateTimeOffset CreatedTime { get; set; } = DateTimeOffset.UtcNow;

        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }
    }

    public enum RState
    {
        Created = 0,
        Accepted = 1,
        Visited = 2,
        Rejected = 3,
        Cancelled = 4
    }
}