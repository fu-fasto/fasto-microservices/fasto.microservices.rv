﻿using MongoDB.Bson.Serialization;

namespace FastO.Microservices.RV.DataModels
{
    public class MongoStructure
    {
        public MongoStructure()
        {
            BsonClassMap.RegisterClassMap<Visit>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Code).SetElementName("code");
                x.GetMemberMap(m => m.StoreId).SetElementName("store_id");
                x.GetMemberMap(m => m.CustomerId).SetElementName("customer_id");
                x.GetMemberMap(m => m.PaymentTransactionId).SetElementName("payment_transaction_id");
                x.GetMemberMap(m => m.StoreName).SetElementName("store_name");
                x.GetMemberMap(m => m.CustomerName).SetElementName("customer_name");
                x.GetMemberMap(m => m.PaymentTransactionState).SetElementName("payment_transaction_state");
                x.GetMemberMap(m => m.PeopleNumber).SetElementName("people_number");
                x.GetMemberMap(m => m.Table).SetElementName("table");
                x.GetMemberMap(m => m.IsActivated).SetElementName("is_activated");
                x.GetMemberMap(m => m.CheckInTime).SetElementName("check_in_time");
                x.GetMemberMap(m => m.CheckOutTime).SetElementName("check_out_time");
            });
            
            BsonClassMap.RegisterClassMap<Reservation>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.Code).SetElementName("code");
                x.GetMemberMap(m => m.StoreId).SetElementName("store_id");
                x.GetMemberMap(m => m.CustomerId).SetElementName("customer_id");
                x.GetMemberMap(m => m.VisitId).SetElementName("visit_id");
                x.GetMemberMap(m => m.StoreName).SetElementName("store_name");
                x.GetMemberMap(m => m.CustomerName).SetElementName("customer_name");
                x.GetMemberMap(m => m.PeopleNumber).SetElementName("people_number");
                x.GetMemberMap(m => m.ExpectedTime).SetElementName("expected_time");
                x.GetMemberMap(m => m.State).SetElementName("state");
                x.GetMemberMap(m => m.Note).SetElementName("note");
                x.GetMemberMap(m => m.States).SetElementName("states");
            });
            
            BsonClassMap.RegisterClassMap<ReservationState>(x =>
            {
                x.AutoMap();
                x.GetMemberMap(m => m.State).SetElementName("state");
                x.GetMemberMap(m => m.Note).SetElementName("note");
                x.GetMemberMap(m => m.CreatedTime).SetElementName("created_time");
            });
        }
    }
}