﻿using System;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Types;

namespace FastO.Microservices.RV.DataModels
{
    public class Visit : EntityBase<Guid>
    {
        /// <summary>
        /// Id of visit
        /// </summary>
        public override Guid Id { get; set; } = Guid.NewGuid();

        /// <summary>
        /// Unique code of visit
        /// </summary>
        [UniqueIndex]
        public string Code { get; set; }
        
        /// <summary>
        /// Id of store
        /// </summary>
        [UniqueIndex]
        public Guid StoreId { get; set; }
        
        /// <summary>
        /// Id of customer
        /// </summary>
        [UniqueIndex]
        public Guid CustomerId { get; set; }

        /// <summary>
        /// Id of payment transaction
        /// </summary>
        public Guid? PaymentTransactionId { get; set; }
        
        /// <summary>
        /// Name of store
        /// </summary>
        public string StoreName { get; set; }
        
        /// <summary>
        /// Name of customer
        /// </summary>
        public string CustomerName { get; set; }

        /// <summary>
        /// State of payment transaction
        /// </summary>
        public bool PaymentTransactionState { get; set; }

        /// <summary>
        /// Number of people
        /// </summary>
        public int PeopleNumber { get; set; }

        /// <summary>
        /// Note of table
        /// </summary>
        public string Table { get; set; }

        /// <summary>
        /// Is activated
        /// </summary>
        public bool IsActivated { get; set; } = true;

        /// <summary>
        /// Check in time
        /// </summary>
        public DateTimeOffset CheckInTime { get; set; }

        /// <summary>
        /// Check out time
        /// </summary>
        public DateTimeOffset? CheckOutTime { get; set; }
    }
}