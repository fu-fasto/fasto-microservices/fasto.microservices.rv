﻿using System;

namespace FastO.Microservices.RV.Dtos
{
    public class PayPalPaymentTransactionResponse
    {
        public Guid Id { get; set; }

        public PaypalTransactionData TransactionData { get; set; }
    }

    public class PaypalTransactionData
    {
        public string PaypalOrderId { get; set; }
    }

    public class MomoPaymentTransactionResponse
    {
        public Guid Id { get; set; }

        public MomoTransactionData TransactionData { get; set; }
    }

    public class MomoTransactionData
    {
        public string MomoOrderId { get; set; }
        public string PayUrl { get; set; }
    }
}