﻿using System;

namespace FastO.Microservices.RV.Dtos
{
    public class UserDto
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}