﻿namespace FastO.Microservices.RV.Dtos
{
    public class CheckInQrDto
    {
        public string Base64Code { get; set; }
        
        public string Type { get; set; }
    }
}