﻿using System;

namespace FastO.Microservices.RV.Dtos
{
    public class CreatePaymentTransactionDto
    {
        public string Provider { get; set; }
        
        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }
        
        public Guid VisitId { get; set; }
        
        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }

        public DateTimeOffset CreateAt { get; set; } = DateTimeOffset.UtcNow;

        public decimal Price { get; set; }

        public string Currency { get; set; }
    }
}