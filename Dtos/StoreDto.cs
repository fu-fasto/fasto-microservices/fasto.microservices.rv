﻿using System;

namespace FastO.Microservices.RV.Dtos
{
    public class StoreDto
    {
        public Guid Id { get; set; }
        
        public string Name { get; set; }
    }
}