﻿using System;

namespace FastO.Microservices.RV.Dtos
{
    public class OrderDto
    {
        public Guid ItemId { get; set; }
        
        public decimal Price { get; set; }

        public int Quantity { get; set; }
        
        public OState State { get; set; }
    }
    
    public enum OState
    {
        Created = 0,
        Accepted = 1,
        Making = 2,
        Done = 3,
        Served = 4,
        Rejected = 5,
        Cancelled = 6
    }
}