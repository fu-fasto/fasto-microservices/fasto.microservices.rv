#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:5.0-buster-slim AS base
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y libgdiplus
RUN apt-get install -y libc6-dev 
RUN ln -s /usr/lib/libgdiplus.so/usr/lib/gdiplus.dll
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:5.0-buster-slim AS build
WORKDIR /src
COPY ["FastO.Microservices.RV.csproj", ""]
RUN dotnet restore "./FastO.Microservices.RV.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "FastO.Microservices.RV.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "FastO.Microservices.RV.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FastO.Microservices.RV.dll"]
