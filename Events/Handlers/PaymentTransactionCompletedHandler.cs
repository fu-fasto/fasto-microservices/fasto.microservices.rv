﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Commands.Handlers;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Events;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Events.Handlers
{
    public class PaymentTransactionCompletedHandler : IEventHandler<PaymentTransactionCompleted>
    {
        private readonly IRepository<Visit, Guid> _visitRepository;
        private readonly ILogger<PaymentTransactionCompletedHandler> _logger;

        public PaymentTransactionCompletedHandler(ILogger<PaymentTransactionCompletedHandler> logger,
            IRepository<Visit, Guid> visitRepository)
        {
            _logger = logger;
            _visitRepository = visitRepository;
        }

        public async Task Handle(PaymentTransactionCompleted paymentTransactionCompleted,
            CancellationToken cancellationToken)
        {
            var visit =
                await _visitRepository.FindOneAsync(paymentTransactionCompleted.VisitId, cancellationToken);
            visit.PaymentTransactionState = true;
            visit.PaymentTransactionId = paymentTransactionCompleted.PaymentTransactionId;
            await _visitRepository.UpdateAsync(visit, cancellationToken);

            _logger.LogInformation("Visit {Id} paid successfully!", visit.Id);
        }
    }
}