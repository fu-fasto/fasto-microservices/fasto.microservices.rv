﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Events
{
    [Message]
    public class VisitCheckedOut : IEvent
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }
        
        public DateTimeOffset CheckOutTime { get; set; }
        
        public bool PaymentTransactionState { get; set; }
    }
}