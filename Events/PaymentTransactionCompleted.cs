﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Events
{
    [Message(MessageScope.External)]
    public class PaymentTransactionCompleted : IEvent
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public Guid StoreId { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public Guid VisitId { get; set; }
        
        public Guid PaymentTransactionId { get; set; }
    }
}