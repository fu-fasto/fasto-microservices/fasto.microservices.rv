﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Events
{
    [Message]
    public class VisitCheckedIn : IEvent
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public string Code { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }
        
        public int PeopleNumber { get; set; }
        
        public string Table { get; set; }

        public DateTimeOffset CheckInTime { get; set; }
    }
}