﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Events
{
    [Message]
    public class ReservationUpdated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public RState State { get; set; }

        public string Note { get; set; }
        
        public bool PaymentTransactionState { get; set; }
    }
}