﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Events
{
    [Message]
    public class ReservationCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }
        
        public string Code { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }
        
        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }

        public int PeopleNumber { get; set; }

        public DateTimeOffset ExpectedTime { get; set; }

        public RState State { get; set; } = RState.Created;

        public string Note { get; set; }
    }
}