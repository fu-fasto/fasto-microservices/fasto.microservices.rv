﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Commands
{
    public class CreateVisitPaymentTransaction : CommandBase
    {
        [MessageKey] public Guid VisitId { get; set; } = Guid.NewGuid();

        public Guid PaymentTransactionId { get; set; }

        public string Provider { get; set; }

        public dynamic TransactionData { get; set; }
    }
}