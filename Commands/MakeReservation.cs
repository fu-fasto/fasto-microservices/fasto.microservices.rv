﻿using System;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Commands
{
    public class MakeReservation : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }
        
        public int PeopleNumber { get; set; }

        public DateTimeOffset ExpectedTime { get; set; }

        public string Note { get; set; }
    }

    public class MakeReservationValidator : AbstractValidator<MakeReservation>
    {
        public MakeReservationValidator()
        {
            RuleFor(command => command.PeopleNumber).GreaterThan(0);
        }
    }
}