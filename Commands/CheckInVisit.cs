﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Commands
{
    public class CheckInVisit : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();
        
        public Guid? ReservationId { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }
        
        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }

        public int PeopleNumber { get; set; }
        
        public string Table { get; set; }

        public DateTimeOffset CheckInTime { get; set; } = DateTimeOffset.UtcNow;
    }
}