﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Commands
{
    public class CheckOutVisit : CommandBase
    {
        [MessageKey] public Guid Id { get; set; } = Guid.NewGuid();

        public DateTimeOffset CheckOutTime { get; set; } = DateTimeOffset.UtcNow;
    }
}