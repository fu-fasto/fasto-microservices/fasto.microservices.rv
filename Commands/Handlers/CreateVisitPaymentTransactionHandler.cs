﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Dtos;
using FastO.Microservices.RV.Services;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Commands.Handlers
{
    public class CreateVisitPaymentTransactionHandler : ICommandHandler<CreateVisitPaymentTransaction>
    {
        private readonly ILogger<CreateVisitPaymentTransactionHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Visit, Guid> _visitRepository;
        private readonly IServiceClient _serviceClient;

        public CreateVisitPaymentTransactionHandler(ILogger<CreateVisitPaymentTransactionHandler> logger,
            IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher, IServiceClient serviceClient)
        {
            _visitRepository = unitOfWork.GetRepository<Visit, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _serviceClient = serviceClient;
            _logger = logger;
        }

        public async Task PreHandle(CreateVisitPaymentTransaction command, CancellationToken cancellationToken)
        {
            var visit = await _visitRepository.FindOneAsync(command.VisitId, cancellationToken) ??
                        throw new ValidationException($"Visit {command.VisitId} not found");
            var orders = await _serviceClient.GetOrdersByVisitId(command.VisitId, cancellationToken);
            var createPaymentTransactionDto = orders.Aggregate<OrderDto, CreatePaymentTransactionDto>(
                new()
                {
                    Currency = "VND",
                    Provider = command.Provider,
                    CustomerId = visit.CustomerId,
                    StoreId = visit.StoreId,
                    VisitId = visit.Id,
                    StoreName = visit.StoreName,
                    CustomerName = visit.CustomerName
                },
                (acc, order) =>
                {
                    switch (order.State)
                    {
                        case OState.Done:
                        case OState.Served:
                            acc.Price += order.Quantity * order.Price;
                            break;
                        case OState.Accepted:
                        case OState.Created:
                        case OState.Making:
                            throw new ValidationException("There are still a few unfinished items");
                        case OState.Cancelled:
                        case OState.Rejected:
                            break;
                    }
                        return acc;
                });

            if (createPaymentTransactionDto.Price == 0)
            {
                throw new ValidationException("Total price is not over zero");
            }

            await _serviceClient.CreatePaymentTransaction(command, createPaymentTransactionDto, cancellationToken);
        }

        public async Task Handle(CreateVisitPaymentTransaction command, CancellationToken cancellationToken)
        {
            var visit = await _visitRepository.FindOneAsync(command.VisitId, cancellationToken);
            visit.PaymentTransactionId = command.PaymentTransactionId;

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _visitRepository.UpdateAsync(visit, cancellationToken);
                // await _messagePublisher.PublishOutBoxAsync(visit.MapTo<VisitCheckedOut>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Visit {Id} create payment transaction successfully!", visit.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}