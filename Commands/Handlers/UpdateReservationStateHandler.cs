﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Events;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Commands.Handlers
{
    public class UpdateReservationStateHandler : ICommandHandler<UpdateReservationState>
    {
        private readonly ILogger<UpdateReservationStateHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IRepository<Reservation, Guid> _reservationRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateReservationStateHandler(ILogger<UpdateReservationStateHandler> logger,
            IUnitOfWork unitOfWork, IMessagePublisher messagePublisher)
        {
            _reservationRepository = unitOfWork.GetRepository<Reservation, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _logger = logger;
        }

        public async Task Handle(UpdateReservationState command, CancellationToken cancellationToken)
        {
            var reservation = await _reservationRepository.FindOneAsync(command.Id, cancellationToken);
            reservation.UpdateReservationState(command.State, command.Note);

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _reservationRepository.UpdateAsync(reservation, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(reservation.MapTo<ReservationUpdated>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);
                
                _logger.LogInformation("Reservation {Id} updated successfully!", reservation.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}