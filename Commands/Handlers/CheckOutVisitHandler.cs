﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Events;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Commands.Handlers
{
    public class CheckOutVisitHandler : ICommandHandler<CheckOutVisit>
    {
        private readonly ILogger<CheckOutVisitHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Visit, Guid> _visitRepository;

        public CheckOutVisitHandler(ILogger<CheckOutVisitHandler> logger, IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher)
        {
            _visitRepository = unitOfWork.GetRepository<Visit, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _logger = logger;
        }

        public async Task Handle(CheckOutVisit command, CancellationToken cancellationToken)
        {
            var visit = await _visitRepository.FindOneAsync(command.Id, cancellationToken);
            visit.CheckOutTime = command.CheckOutTime;
            visit.IsActivated = false;
            
            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _visitRepository.UpdateAsync(visit, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(visit.MapTo<VisitCheckedOut>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Visit {Id} checked out successfully!", visit.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}