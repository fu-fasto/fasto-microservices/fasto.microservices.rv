﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Events;
using FastO.Microservices.RV.Services;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Commands.Handlers
{
    public class CheckInVisitHandler : ICommandHandler<CheckInVisit>
    {
        private readonly ILogger<CheckInVisitHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Reservation, Guid> _reservationRepository;
        private readonly IRepository<Visit, Guid> _visitRepository;
        private readonly IServiceClient _serviceClient;

        public CheckInVisitHandler(ILogger<CheckInVisitHandler> logger, IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher, IServiceClient serviceClient)
        {
            _visitRepository = unitOfWork.GetRepository<Visit, Guid>();
            _reservationRepository = unitOfWork.GetRepository<Reservation, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _serviceClient = serviceClient;
            _logger = logger;
        }

        public async Task PreHandle(CheckInVisit command, CancellationToken cancellationToken)
        {
            if (command.ReservationId.HasValue)
            {
                var reservation =
                    await _reservationRepository.FindOneAsync(command.ReservationId.Value, cancellationToken)
                    ?? throw new NullReferenceException($"Reservation {command.ReservationId} not found!");
                command.StoreId = reservation.StoreId;
                command.CustomerId = reservation.CustomerId;
                command.StoreName = reservation.StoreName;
                command.CustomerName = reservation.CustomerName;
            }
            else
            {
                var store = await _serviceClient.GetStore(command.StoreId, cancellationToken) ??
                            throw new ValidationException($"Store {command.StoreId} not exist!");
                var user = await _serviceClient.GetUser(command.CustomerId, cancellationToken) ??
                           throw new ValidationException($"Customer {command.CustomerId} not exist!");

                command.StoreName = store.Name;
                command.CustomerName = user.Name;
            }
        }

        public async Task Handle(CheckInVisit command, CancellationToken cancellationToken)
        {
            var visit = command.MapTo<Visit>();

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                if (command.ReservationId.HasValue)
                {
                    var reservation =
                        await _reservationRepository.FindOneAsync(command.ReservationId.Value, cancellationToken);
                    reservation.UpdateReservationState(RState.Visited);
                    await _reservationRepository.UpdateAsync(reservation, cancellationToken);
                    await _messagePublisher.PublishOutBoxAsync(reservation.MapTo<ReservationUpdated>(),
                        cancellationToken);
                }

                do
                {
                    visit.Code = MicroBoostHelpers.RandomCode(8);
                } while (await _visitRepository.IsExistsAsync(x => x.Code.Equals(visit.Code), cancellationToken));

                await _visitRepository.AddAsync(visit, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(visit.MapTo<VisitCheckedIn>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Visit {Id} checked in successfully!", visit.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}