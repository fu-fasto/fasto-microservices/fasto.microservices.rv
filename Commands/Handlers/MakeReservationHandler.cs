﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Events;
using FastO.Microservices.RV.Services;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.RV.Commands.Handlers
{
    public class MakeReservationHandler : ICommandHandler<MakeReservation>
    {
        private readonly ILogger<MakeReservationHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IRepository<Reservation, Guid> _reservationRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServiceClient _serviceClient;

        public MakeReservationHandler(ILogger<MakeReservationHandler> logger, IUnitOfWork unitOfWork,
            IMessagePublisher messagePublisher, IServiceClient serviceClient)
        {
            _reservationRepository = unitOfWork.GetRepository<Reservation, Guid>();
            _unitOfWork = unitOfWork;
            _messagePublisher = messagePublisher;
            _serviceClient = serviceClient;
            _logger = logger;
        }

        public async Task PreHandle(MakeReservation command, CancellationToken cancellationToken)
        {
            var store = await _serviceClient.GetStore(command.StoreId, cancellationToken) ??
                        throw new ValidationException($"Store {command.StoreId} not exist!");
            var user = await _serviceClient.GetUser(command.CustomerId, cancellationToken) ??
                       throw new ValidationException($"Customer {command.CustomerId} not exist!");

            command.StoreName = store.Name;
            command.CustomerName = user.Name;
        }

        public async Task Handle(MakeReservation command, CancellationToken cancellationToken)
        {
            var reservation = command.MapTo<Reservation>();
            reservation.UpdateReservationState(RState.Created, command.Note);

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                do
                {
                    reservation.Code = MicroBoostHelpers.RandomCode(8);
                } while (await _reservationRepository.IsExistsAsync(x => x.Code.Equals(reservation.Code), cancellationToken));
                
                await _reservationRepository.AddAsync(reservation, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(reservation.MapTo<ReservationCreated>(), cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Reservation {Id} created successfully!", reservation.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}