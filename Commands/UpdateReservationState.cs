﻿using System;
using FastO.Microservices.RV.DataModels;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.RV.Commands
{
    public class UpdateReservationState : CommandBase
    {
        [MessageKey] public Guid Id { get; set; }
        public RState State { get; set; }
        public string Note { get; set; }
    }
}