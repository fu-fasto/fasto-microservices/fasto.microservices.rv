using FastO.Microservices.RV.DataModels;
using FastO.Microservices.RV.Services;
using MicroBoost;
using MicroBoost.Cqrs;
using MicroBoost.Http;
using MicroBoost.Jaeger;
using MicroBoost.MessageBroker;
using MicroBoost.Metrics;
using MicroBoost.Persistence;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace FastO.Microservices.RV
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMicroBoostBuilder()
                .AddJaeger()
                .AddWebApi()
                .AddCqrs()
                .AddNoSqlPersistence()
                .AddMessageBroker()
                .AddSwaggerDocs()
                .AddHttpClient()
                .AddSingleton<IServiceClient, ServiceClient>()
                .AddSingleton<MongoStructure>()
                .Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app
                .UseMetrics()
                .UseJaeger()
                .UseWebApi()
                .UseMessageBroker()
                .UseSwaggerDocs();
        }
    }
}