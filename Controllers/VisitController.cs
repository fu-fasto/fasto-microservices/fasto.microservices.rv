﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Commands;
using FastO.Microservices.RV.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.RV.Controllers
{
    [ApiController]
    [Route("visits")]
    public class VisitController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public VisitController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet("check-in-qr")]
        public async Task<IActionResult> GetCheckInQr([FromQuery] GetCheckInQr query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }
        
        [HttpGet]
        public async Task<IActionResult> GetAllVisits([FromQuery] GetAllVisits query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneVisit([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneVisit {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CheckInVisit([FromBody] CheckInVisit command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPatch("{id}/checkout")]
        public async Task<IActionResult> CheckOutVisit([FromRoute] Guid id,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(new CheckOutVisit {Id = id}, cancellationToken);
            return Accepted(id);
        }

        [HttpPost("{id}/create-payment-transaction")]
        public async Task<IActionResult> CreateVisitPaymentTransaction([FromRoute] Guid id,
            [FromBody] CreateVisitPaymentTransaction command,
            CancellationToken cancellationToken)
        {
            command.VisitId = id;
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command);
        }
    }
}