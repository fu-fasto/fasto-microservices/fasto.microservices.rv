﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.RV.Commands;
using FastO.Microservices.RV.Queries;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.RV.Controllers
{
    [ApiController]
    [Route("reservations")]
    public class ReservationController : ControllerBase
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public ReservationController(ICommandBus commandBus, IQueryBus queryBus)
        {
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllReservations([FromQuery] GetAllReservations query,
            CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(query, cancellationToken);
            return Ok(result);
        }

        // [HttpGet("browse")]
        // public async Task<IActionResult> BrowseReservations([FromQuery] BrowseReservations query,
        //     CancellationToken cancellationToken)
        // {
        //     var result = await _queryBus.SendAsync(query, cancellationToken);
        //     return Ok(result);
        // }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneReservation([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneReservation {Id = id}, cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> MakeReservation([FromBody] MakeReservation command,
            CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(command.Id);
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateReservationState([FromRoute] Guid id,
            [FromBody] UpdateReservationState command,
            CancellationToken cancellationToken)
        {
            command.Id = id;
            await _commandBus.SendAsync(command, cancellationToken);
            return Accepted(id);
        }
    }
}